# CML basic use case

This repository contains a sample project using [CML](https://github.com/iterative/cml). When a pull request is made in this repository, the following will occur:
- Bitbucket Cloud will deploy a runner machine with a specified CML Docker environment
- The runner will execute a workflow to train a ML model (`python train.py`)
- A visual CML report about the model performance will be returned as a comment on the triggering commit. 

The key file enabling these actions is `bitbucket-pipelines.yml`.

## Repository variables
You must set the repository variable `repo_token` to authorize CML to leave comments on your repository. Because the API requires a username and password, you have two options for configuring `repo_token`:

1. Use the access credentials from a user on your team (note that you may also consider using [Bitbucket Cloud App Passwords](https://support.atlassian.com/bitbucket-cloud/docs/app-passwords/) to generate a password just for CML, if you would prefer).
2. Create a designated "CI/CD" account to author CML reports.

Whichever you choose, the steps to create your `repo_token` will be the same:

1. Use a Base64 encoder of your choice to encode the string, `username:password`. For example, `printf "$USERNAME:$PASSWORD" | base64`.
2. In your repository, go to **Repository Settings** -> **Repository Variables**.
3. In the field "Name", enter `repo_token`. 
4. In the field "Value", enter your Base64 transformed string. 
5. Check `Secured` to ensure that your credentials are hidden in all Bitbucket Pipelines logs. 
